import { connect } from 'react-redux'
import SignUpForm from './SignUpForm'
import { signUpUser } from '../../state/actions/SignUpFormActions'

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSignUpFormSubmit: (name, isOwner) => {
      dispatch(signUpUser(name, isOwner))
    }
  }
}

const SignUpFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpForm)

export default SignUpFormContainer
