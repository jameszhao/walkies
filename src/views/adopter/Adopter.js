import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { getAllDogs, adoptDog } from '../../state/actions/DogActions';

class Adopter extends Component {
  constructor(props, { authData }) {
    super(props);
    authData = this.props;
    this.state = {
      selectedDog: null,
    };
  }

  componentWillMount() {
    this.props.getAllDogs();
  }
  
  componentWillUnmount() {
  }

  selectDog = (dogId) => {
    if(this.props.dogs && this.props.dogs.length) {
      const selected = this.props.dogs.find(dog => {
        return dog.id === dogId;
      });
      this.setState({
        selectedDog: selected,
      });
    }
  }

  adopt = () => {
    this.props.adoptDog(this.state.selectedDog.id);
  }

  render() {
    const { dogs, authData } = this.props;
    const { selectedDog } = this.state;
    return (
      <div className="dashboard-container">
        <div className="dashboard-left-panel">
          <h1>{`Welcome ${authData.userName}`}</h1>
          <div style={{fontSize: 12}}>Your address: { this.props.authData.address }</div>
          <p>Please adopt a dog in the following list.</p>
          <button className="refresh" onClick={this.props.getAllDogs}>Refresh list</button>
          <div className="pets-container">
            {dogs.map(dog => (
              <div key={dog.id} style={{ opacity: (dog.status === 'created') ? 1 : 0.5 }} onClick={() => this.selectDog(dog.id)} className={"dog-container"}>
                <div className={"name"}>{dog.name}</div>
                <div className={"breed"}>{dog.breed}</div>
              </div>
            ))}
          </div>
        </div>
        <div className="dashboard-right-panel">
        {
          selectedDog && 
            <div className="dog-info">
              <div>
                <span className="label">ID:</span> {selectedDog.id}
              </div>
              <div>
                <span className="label">Owner:</span> {selectedDog.owner}
              </div>
              <div>
                <span className="label">Name:</span> {selectedDog.name}
              </div>
              <div>
                <span className="label">Breed:</span> {selectedDog.breed}
              </div>
              <div>
                <span className="label">Status:</span> {selectedDog.status}
              </div>
              {
                selectedDog.status === 'created' && <button onClick={this.adopt}>Adopt!</button>
              }
              
            </div>
        }          
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    dogs: state.dog.dogs,
  }),
  {
    pushTo: push,
    adoptDog,
    getAllDogs,
  },
)(Adopter);
