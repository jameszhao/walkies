var Ownable = artifacts.require("zeppelin/ownership/Ownable.sol");
var Destructible = artifacts.require("zeppelin/lifecycle/Destructible.sol");
var Users = artifacts.require("./Users.sol");
var Dogs = artifacts.require("./Dogs.sol");

module.exports = function(deployer) {
  deployer.deploy(Ownable);
  deployer.deploy(Destructible);
  deployer.deploy(Users);
  deployer.deploy(Dogs);
};
