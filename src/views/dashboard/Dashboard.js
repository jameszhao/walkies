import React, { Component } from "react";
import { push } from "react-router-redux";
import { connect } from 'react-redux'

import "./styles.css";

class Dashboard extends Component {
  constructor(props, { authData }) {
    super(props);
    authData = this.props;
  }

  componentDidMount() {
    const { authData, pushTo } = this.props;
    if (authData.userType === 'owner') {
      pushTo('/dashboard/owner');
    } else if (authData.userType === 'adopter') {
      pushTo('/dashboard/adopter');
    }
  }

  componentDidUpdate() {
    if (this.props.location.pathname === '/dashboard') {
      const { authData, pushTo } = this.props;
      if (authData.userType === 'owner') {
        pushTo('/dashboard/owner');
      } else if (authData.userType === 'adopter') {
        pushTo('/dashboard/adopter');
      }
    }
  }  

  render() {
    return (
      <main className="container">
        {this.props.children}
      </main>
    );
  }
}

export default connect(
  null,
  {
    pushTo: push
  }
)(Dashboard);
