var Dogs = artifacts.require("./Dogs.sol");

contract('Dogs', function(accounts) {
  let deployedInstance;

  beforeEach(async () => {
    deployedInstance = await Dogs.deployed()
  })

  it("should add dog correctly.", async() => {
    const instance = await Dogs.new();
    await instance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    const dogId = await instance.getDog.call(0);
    assert.equal(dogId[0], 'Sunny', "The dog name was not added correctly.");
    assert.equal(dogId[1], 'Labrador', "The dog breed was not added correctly.");
    const dogIds = await instance.dogIds(0);
    assert.equal(web3.toDecimal(dogIds), 0, "The dog id was not added correctly.");
  });


  it("add dog returns correct id.", async() => {
    const instance = await Dogs.new();
    await instance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    let tx = await instance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    assert.equal(tx.logs[0].args.dogId, 1, "Added dog id starts with 0.");
    tx = await instance.addDog('Gizmo', 'Bulldog', {from: accounts[0]})
    assert.equal(tx.logs[0].args.dogId, 2, "Added dog id starts with 0.");
  });

  it("should increment ids correctly.", async() => {
    await deployedInstance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    const dogIds = await deployedInstance.dogIds(0);
    assert.equal(web3.toDecimal(dogIds), 0, "The dog id was incremented correctly.");
    await deployedInstance.addDog('Honey', 'Cockerspaniel', {from: accounts[0]})
    const dogIds2 = await deployedInstance.dogIds(1);
    assert.equal(web3.toDecimal(dogIds2), 1, "The dog id was incremented correctly.");
    await deployedInstance.addDog('Neo', 'Dalmation', {from: accounts[0]})
    const dogIds3 = await deployedInstance.dogIds(2);
    assert.equal(web3.toDecimal(dogIds3), 2, "The dog id was incremented correctly.");
  });
  
  it("should have correct number of ids stored", async() => {
    const dogCount = await deployedInstance.getDogCount();
    assert.equal(web3.toDecimal(dogCount), 3, "The dog id was incremented correctly.");
  })

  it("should return correct dog ids when fetching all ids", async() => {
    const instance = await Dogs.new();
    await instance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    await instance.addDog('Sunny', 'Labrador', {from: accounts[1]})
    const dogIds = await instance.getAllDogIds();
    assert.equal(dogIds[0], 0, "The correct dog ids come back for sender.");
    assert.equal(dogIds.length, 2, "The correct dog ids come back for sender.");
  })

  it("should return correct dog ids when fetching owner ids", async() => {
    const instance = await Dogs.new();
    await instance.addDog('Sunny', 'Labrador', {from: accounts[0]})
    await instance.addDog('Sunny', 'Labrador', {from: accounts[1]})
    const dogIds = await instance.getOwnerDogIds();
    assert.equal(dogIds[0], 0, "The correct dog ids come back for sender.");
    assert.equal(dogIds.length, 1, "The correct dog ids come back for sender.");
  })
});
