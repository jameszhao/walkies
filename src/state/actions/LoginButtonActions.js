import Users from '../../../build/contracts/Users.json';
import { hashHistory } from 'react-router';
import { getWeb3Instance } from './web3helper';

export const USER_LOGGED_IN = 'USER_LOGGED_IN';
function userLoggedIn(user) {
  return {
    type: USER_LOGGED_IN,
    payload: user,
  };
}

function loginUserCall({ instance: usersInstance, coinbase, web3 }, dispatch) {
  // Attempt to login user.
  usersInstance
    .login({ from: coinbase })
    .then(function(result) {
      // If no error, login user.
      var userName = result[0];
      var userType = result[1];
      dispatch(userLoggedIn({ userName, userType, address: web3.eth.accounts[0] }));

      // Used a manual redirect here as opposed to a wrapper.
      // This way, once logged in a user can still access the home page.
      var currentLocation = hashHistory.getCurrentLocation();

      if ('redirect' in currentLocation.query) {
        return hashHistory.push(
          decodeURIComponent(currentLocation.query.redirect),
        );
      }

      return hashHistory.push('/dashboard');
    })
    .catch(function(result) {
      // If error, go to signup page.
      console.error('Wallet ' + coinbase + ' does not have an account!');
      console.error(result);
      return hashHistory.push('/signup');
    });
}

export function loginUser() {
  return getWeb3Instance(Users, loginUserCall);
}
