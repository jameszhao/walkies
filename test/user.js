var Users = artifacts.require("./Users.sol");

contract('Users', function(accounts) {
  let deployedInstance;

  beforeEach(async () => {
    deployedInstance = await Users.deployed()
  })

  it("sign up increases user count.", async () => {
    const instance = await Users.new();
    await instance.signup('Michael Jordan', false, {from: accounts[1]})
    await instance.signup('Kobe', true, {from: accounts[2]})
    const addresses = await instance.getNumberOfUsers();
    assert.equal(addresses, 2, "The user name was not signed up correctly.");
  });

  it("should sign up with correct user type", async () => {
    const instance = await Users.new();
    await instance.signup('Kobe Bryant', false, {from: accounts[0]})
    const user = await instance.login.call();
    assert.equal(user[1], "adopter", "The user type was not signed up correctly.");
  });

  it("should sign up with correct name", async () => {
    await deployedInstance.signup('Michael Jordan', true, {from: accounts[0]})
    const user = await deployedInstance.login.call();
    assert.equal(user[0], "Michael Jordan", "The user name was not signed up correctly.");
  });


  it("should not overwrite existing user", async () => {
    const instance = await Users.new();
    await instance.signup('Kobe Bryant', false, {from: accounts[0]})
    await instance.signup('Michael Jordan', false, {from: accounts[0]})
    const user = await instance.login({from: accounts[0]});
    assert.equal(user[0], "Kobe Bryant", "The user type was not signed up correctly.");
  });

  it("should sign in successfully with different accounts", async () => {
    const instance = await Users.new();
    await instance.signup('Kobe Bryant', false, {from: accounts[1]})
    await instance.signup('Michael Jordan', false, {from: accounts[2]})
    const user = await instance.login({from: accounts[2]});
    assert.equal(user[0], "Michael Jordan", "The user type was not signed up correctly.");
  });

  // it("...should sign up and log in a dog owner.", function() {
  //     return instance.signup('Michael Jordan', true, {from: accounts[1]})
  //   .then(function() {
  //     return instance.login.call();
  //   }).then(function(user) {
  //     assert.equal(user[0], "Kobe Bryant", "The user name was not signed up correctly.");
  //     assert.equal(user[1], "owner", "The user type was not signed up correctly.");
  //   });
  // });

  // it("...should not work if name is not defined.", function() {
  //   return Users.deployed().then(function(instance) {
  //     usersInstance = instance;

  //     return usersInstance.signup("", true, {from: accounts[0]});
  //   }).then(function() {
  //     return usersInstance.login.call();
  //   }).then(function(user) {
  //     assert.equal(user[0], "Michael Jordan", "The user name was not signed up correctly.");
  //     assert.equal(user[1], "walker", "The user type was not signed up correctly.");
  //   });
  // });

});
