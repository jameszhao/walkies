import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDog } from '../../state/actions/DogActions';
import './styles.css';

class DogInfo extends Component {
  constructor(props, { authData }) {
    super(props);
    authData = this.props;
  }

  componentWillMount() {
    this.props.getDog(this.props.params.dogId);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.params.dogId !== this.props.params.dogId) {
      this.props.getDog(this.props.params.dogId);
    }
  }

  render() {
    const { dog } = this.props;
    return (
      <div>
        {dog && (
          <div className="dog-info">
            <h2>
              <span className="label">ID:</span> {dog.id}
            </h2>
            <h2>
              <span className="label">Owner:</span> {dog.owner}
            </h2>
            <h2>
              <span className="label">Name:</span> {dog.name}
            </h2>
            <h2>
              <span className="label">Breed:</span> {dog.breed}
            </h2>
            <h2>
              <span className="label">Status:</span> {dog.status}
            </h2>
          </div>
        )}
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    dog: state.dog.selectedDog,
  }),
  {
    getDog,
  },
)(DogInfo);
