# Using Tested Contracts/Libraries
I have extensively used EthPM and tested contracts/libraries that have been verified by experts in the field, such as EIP20 token and Zeppelin. Instead of creating new implementations this provides the contracts with the best chance of best practices and avoiding security vulnerabilities

# Extensive use of modifiers/require statements
These are used to ensure valid data before changes are attempted and have been used extensively through all contracts.

## SafeMath
To avoid Integer Overflow/Underflow issues I have used the Zeppelin SafeMath library that will throw an error if any possible side effects might be caused.

## Timestamp dependence
None of my contracts have a dependency on a timestamp avoiding this problem. 