import { GOT_DOGS, GOT_SELECTED_DOG, GOT_ALL_DOGS } from '../actions/DogActions';

const initialState = {
  myDogs: [],
  selectedDog: null,
  dogs: [],
}

const dogsReducer = (state = initialState, action) => {
  if (action.type === GOT_DOGS)
  {
    return Object.assign({}, state, {
      myDogs: action.dogs
    })
  }
  if (action.type === GOT_SELECTED_DOG)
  {
    return Object.assign({}, state, {
      selectedDog: action.dog
    })
  }
  if (action.type === GOT_ALL_DOGS)
  {
    return Object.assign({}, state, {
      dogs: action.dogs
    })
  }

  return state
}

export default dogsReducer
