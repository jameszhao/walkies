var HDWalletProvider = require("truffle-hdwallet-provider");

console.log(process.env.WALLET_MNEMONIC_RINKEBY);
console.log(process.env.INFURA_API_KEY);

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      network_id: 4,
      provider: function () {
        return new HDWalletProvider(process.env.WALLET_MNEMONIC_RINKEBY, `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`)
      }
    }
  }
};
