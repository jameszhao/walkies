import React, { Component } from "react";
import { connect } from 'react-redux'
import { push } from "react-router-redux";

import { getDogs, addDog } from "../../state/actions/DogActions";

class Owner extends Component {
  constructor(props, { authData }) {
    super(props);
    authData = this.props;
    this.state = {
      dogBreed: '',
      dogName: '',
    };
  }

  componentWillMount() {
    this.props.getDogs();
    this.timer = setInterval(()=> this.props.getDogs(), 1000);
  }
  
  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  addPet = () => {
    const { dogBreed, dogName } = this.state;
    this.props.addDog(dogName, dogBreed);
  };

  handleDogNameChange = event => {
    this.setState({ dogName: event.target.value });
  };

  handleDogBreedChange = event => {
    this.setState({ dogBreed: event.target.value });
  };

  render() {
    const { authData, dogs, pushTo } = this.props;
    return (
      <div className="dashboard-container">
        <div className="dashboard-left-panel">
          <h1>{`${authData.userName} dogs`}</h1>
          <div style={{fontSize: 12}}>Your address: { this.props.authData.address }</div>
          <p>Add dogs up for adoption here:</p>
          <div className="add-pet-container">
            <input
              placeholder="Name"
              type="text"
              value={this.state.dogName}
              onChange={e => this.handleDogNameChange(e)}
            />
            <input
              placeholder="Breed"
              type="text"
              value={this.state.dogBreed}
              onChange={e => this.handleDogBreedChange(e)}
            />
            <button onClick={this.addPet}>Add dog</button>
          </div>
          <div className="dogs-container">
          {
            dogs.map(dog => (
              <div key={dog.id} style={{ opacity: (dog.status === 'created') ? 1 : 0.5 }} onClick={() => pushTo(`/dashboard/owner/${dog.id}`)} className={"dog-container"}>
                <div className={"name"}>{dog.name}</div>
                <div className={"breed"}>{dog.breed}</div>
              </div>
            ))
          }
          </div>
        </div>
        <div className="dashboard-right-panel">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    dogs: state.dog.myDogs,
  }),
  {
    pushTo: push,
    addDog,
    getDogs,
  }
)(Owner);
