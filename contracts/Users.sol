pragma solidity ^0.4.2;

import "zeppelin/lifecycle/Destructible.sol";
import "zeppelin/math/SafeMath.sol";

/** @title Users Contract. */
contract Users is Destructible {
  using SafeMath for uint;

  /** @dev User Struct. */
  struct User {
    string name;
    string userType;
  }
  
  uint private idCounter = 0;
  address[] public userAddresses;
  mapping (address => User) private users;

  modifier onlyExistingUser {
    // Check if user exists or terminate
    require(bytes(users[msg.sender].name).length > 0, "Only existing user");
    _;
  }

  modifier onlyValidName(string name) {
    // Only valid names allowed

    require(bytes(name).length != 0, "Name cannot be empty.");
    _;
  }


  /** @dev Checks whether sender exists and returns user details
    * @return n User's name.
    * @return t User type: Owner or Adopter.
    */
  function login()
  public
  view
  onlyExistingUser
  returns (string n, string t) {
    return (users[msg.sender].name, users[msg.sender].userType);
  }


  /** @dev Registers user.
    * @param name Name of the user.
    * @param isOwner Is the user an owner.
    * @return n User's name.
    * @return t User type: Owner or Adopter.
    */
  function signup(string name, bool isOwner)
  public
  payable
  onlyValidName(name)
  returns (string n, string t) {
    string memory userType = "owner";
    if (!isOwner) {
      userType = "adopter";
    }
    if (bytes(users[msg.sender].name).length == 0)
    {
      userAddresses.push(msg.sender);
      users[msg.sender].name = name;
      users[msg.sender].userType = userType;

      return (users[msg.sender].name, users[msg.sender].userType);
    }

    return (users[msg.sender].name, users[msg.sender].userType);
  }


  /** @dev Gets total number of users.
    * @return Number of users.
    */
  function getNumberOfUsers()
  public
  view
  returns (uint) {
    return userAddresses.length;
  }
}
