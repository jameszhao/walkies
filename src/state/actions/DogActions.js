import Dogs from '../../../build/contracts/Dogs.json';
import { getWeb3Instance } from './web3helper';

export const GOT_DOGS = 'GOT_DOGS';
export const GOT_ALL_DOGS = 'GOT_ALL_DOGS';
export const GOT_SELECTED_DOG = 'GOT_SELECTED_DOG';

function gotUserDogs(account, dogs) {
  return {
    type: GOT_DOGS,
    account,
    dogs,
  };
}
function gotAllDogs(dogs) {
  return {
    type: GOT_ALL_DOGS,
    dogs,
  };
}
function gotSelectedDog(account, dog) {
  return {
    type: GOT_SELECTED_DOG,
    dog,
  };
}

async function adoptDogCall(
  { instance: dogInstance, coinbase, web3 },
  dispatch,
  { id },
) {
  await dogInstance.adoptDog(id, { from: coinbase });
  dispatch(getAllDogs());
}

async function addDogCall(
  { instance: dogInstance, coinbase, web3 },
  dispatch,
  { name, breed },
) {
  await dogInstance.addDog(name, breed, { from: coinbase });
  dispatch(getDogs());
}

async function getOwnerDogs({instance: dogInstance, web3}, dispatch, coinbase) {
  const result = await dogInstance.getOwnerDogIds({ from: web3.eth.accounts[0] });
  const dogIds = (result && result.length) ? result.map(bigNumber => web3.toDecimal(bigNumber)) : [];
  const dogs = [];
  for(let i = 0; i < dogIds.length; i++) {
    const dog = await dogInstance.getDog(dogIds[i]);
    dogs.push({
      id: dogIds[i],
      name: dog[0],
      breed: dog[1],
      status: dog[2],
      owner: dog[3]
    });
  }
  dispatch(gotUserDogs(web3.eth.accounts[0], dogs));
}

async function getAllDogsCall({instance: dogInstance, web3}, dispatch, coinbase) {
  const result = await dogInstance.getAllDogIds.call();
  const dogIds = (result && result.length) ? result.map(bigNumber => web3.toDecimal(bigNumber)) : [];
  const dogs = [];
  for(let i = 0; i < dogIds.length; i++) {
    const dog = await dogInstance.getDog.call(dogIds[i]);
    dogs.push({
      id: dogIds[i],
      name: dog[0],
      breed: dog[1],
      status: dog[2],
      owner: dog[3]
    });
  }
  dispatch(gotAllDogs(dogs));
}

async function getDogCall({instance: dogInstance, web3}, dispatch, { id }) {
  const dogResult = await dogInstance.getDog.call(id);
  const dog = {
    id,
    name: dogResult[0],
    breed: dogResult[1],
    status: dogResult[2],
    owner: dogResult[3],
  };
  dispatch(gotSelectedDog(web3.eth.accounts[0], dog));
}

export function addDog(name, breed) {
  return getWeb3Instance(Dogs, addDogCall, { name, breed });
}

export function getDogs() {
  return getWeb3Instance(Dogs, getOwnerDogs);
}

export function getAllDogs() {
  return getWeb3Instance(Dogs, getAllDogsCall);
}

export function getDog(id) {
  return getWeb3Instance(Dogs, getDogCall, { id });
}
export function adoptDog(id) {
  return getWeb3Instance(Dogs, adoptDogCall, { id });
}
