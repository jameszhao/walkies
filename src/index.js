import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { UserIsAuthenticated, UserIsNotAuthenticated } from './util/wrappers.js'
import getWeb3 from './util/web3/getWeb3'
import OwnerPage from './views/owner/Owner'
import AdopterPage from './views/adopter/Adopter'
import DogInfoPage from './views/owner/DogInfo'
import { UserIsOwner, UserIsAdopter } from './util/wrappers.js'

// Layouts
import App from './App'
import Home from './views/home/Home'
import Dashboard from './views/dashboard/Dashboard'
import SignUp from './views/signup/SignUp'
import Profile from './views/profile/Profile'

// Redux Store
import store from './store'

// Initialize react-router-redux.
const history = syncHistoryWithStore(hashHistory, store)

// Initialize web3 and set in Redux.
getWeb3
.then(results => {
  console.log('Web3 initialized!')
})
.catch(() => {
  console.log('Error in web3 initialization.')
})

ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Home} />
          <Route path="dashboard" component={UserIsAuthenticated(Dashboard)}>
            <Route path={`/dashboard/owner`} component={UserIsOwner(OwnerPage)}>
              <Route path={`/dashboard/owner/:dogId`} component={DogInfoPage} />
            </Route>
            <Route path={`/dashboard/adopter`} component={UserIsAdopter(AdopterPage)} />
          </Route>
          <Route path="signup" component={UserIsNotAuthenticated(SignUp)} />
          <Route path="profile" component={UserIsAuthenticated(Profile)} />
        </Route>
      </Router>
    </Provider>
  ),
  document.getElementById('root')
)
