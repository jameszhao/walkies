import React, { Component } from 'react'
import './styles.css';

class SignUpForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: ''
    }
  }

  onInputChange(event) {
    this.setState({ name: event.target.value })
  }

  registerAsOwner = (event) => {
    event.preventDefault();
    this.handleSubmit(true);

  }

  registerAsAdopter = (event) => {
    event.preventDefault();
    this.handleSubmit(false);
  }

  handleSubmit(isOwner) {
    if (this.state.name.length < 2)
    {
      return alert('Please fill in your name.')
    }

    this.props.onSignUpFormSubmit(this.state.name, isOwner)
  }

  render() {
    return(
      <div>
        <label htmlFor="name">Name</label>
        <input id="name" type="text" value={this.state.name} onChange={this.onInputChange.bind(this)} placeholder="Name" />
        <span className="pure-form-message">This is a required field.</span>
        <br />

        <button className="pure-button pure-button-primary reg-button" onClick={this.registerAsOwner}>I'd like to find a home</button>
        <button className="pure-button pure-button-primary" onClick={this.registerAsAdopter}>I'd like to adopt</button>
      </div>
    )
  }
}

export default SignUpForm
