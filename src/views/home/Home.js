import React, { Component } from 'react'

class Home extends Component {
  render() {
    return(
      <main className="main-container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Welcome to Dog Adoptors!</h1>
            <p>This is a simple market place for people to put their dogs up for adoption.</p>
            <p>Putting a dog up for adoption and adopting a dog only requires the users to pay in ether for the transaction.</p>
          </div>
        </div>
      </main>
    )
  }
}

export default Home
