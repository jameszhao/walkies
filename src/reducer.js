import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './state/reducers/userReducer'
import dogsReducer from './state/reducers/dogsReducer'
import web3Reducer from './state/reducers/web3Reducer'

const reducer = combineReducers({
  routing: routerReducer,
  user: userReducer,
  web3: web3Reducer,
  dog: dogsReducer,
})

export default reducer
