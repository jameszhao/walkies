import Users from '../../../build/contracts/Users.json';
import { loginUser } from './LoginButtonActions';
import { getWeb3Instance } from './web3helper';

function signUpUserCall(
  { instance: usersInstance, coinbase },
  dispatch,
  { name, isOwner },
) {
  usersInstance
    .signup(name, isOwner, { from: coinbase })
    .then(function(result) {
      // If no error, login user.
      return dispatch(loginUser());
    })
    .catch(function(result) {
      console.error(result);
    });
}

export function signUpUser(name, isOwner) {
  return getWeb3Instance(Users, signUpUserCall, { name, isOwner });
}
