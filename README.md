# What does your project do?
The project allows people to put their dogs up for adoption. You can sign up as a dog owner and add pets. Or sign up as an adopter, view all dogs, and adopt them.

# Run project
- Make sure you have a local blockchain running on port 8545, I've set up Ganache to run at `http://127.0.0.1:8545` with metamask pointing to it.
- `npm i`
- `npm start` will start the web project
- `truffle migrate`
- Go to http://localhost:3000

# Run tests
- `truffle test`

# User Stories
- As a dog owner I can sign up and add my dog to a list for adoption.
- As a dog walker I can sign up and adopt a dog.

# Workflow
- Sign up with an account in metamask as the dog owner.
- Add dogs by going to the dashboard and entering name and breed of the dog.
- Sign out and change account in metamask.
- Sign up as an adopter.
- Click on dogs and click adopt to adopt the dog.
- Once adopted, wait a bit and click refresh. You should see the link to the dog grey out.

Note: updates take a couple seconds so please wait after adding or adopting a dog for UI updates.


# Rinkeby and IPFS
The contracts are published to the Rinkeby Test Network and the Dapp is hosted on IPFS here:

https://ipfs.io/ipfs/Qmd96t6qdLkenVb1Ft1MJr6Fu3XWi4sfucrwPECpew1q6w


