import store from '../../store';
import contract from 'truffle-contract';

export function getWeb3Instance(Contract, callback, args) {
  let web3 = store.getState().web3.web3Instance;
  if (typeof web3 !== 'undefined') {
    return function(dispatch) {
      // Using truffle-contract we create the users object.
      const genericContract = contract(Contract);
      genericContract.setProvider(web3.currentProvider);

      // Get current ethereum wallet.
      web3.eth.getCoinbase((error, coinbase) => {
        // Log errors, if any.
        if (error) {
          console.error(error);
        }

        genericContract.deployed().then(async function(instance) {
          await callback({ instance, coinbase, web3 }, dispatch, args);
        });
      });
    };
  } else {
    console.error('Web3 is not initialized.');
  }
}
