pragma solidity ^0.4.2;

import "zeppelin/lifecycle/Destructible.sol";
import "zeppelin/math/SafeMath.sol";

/** @title Dogs Contract. */
contract Dogs is Destructible {
  using SafeMath for uint;

  enum AdoptionStatus { Created, Adopted }
  
  /** @dev Dog Struct. */
  struct Dog {
    address owner;
    string name;
    string breed;
    AdoptionStatus status;
  }

  uint private idCounter = 0;
  uint[] public dogIds;
  mapping (uint => Dog) private dogs;
  mapping (address => uint[]) public ownerDogs;

  /** @dev Modifier to check if dog belongs to sender. */
  modifier dogBelongsToSender(uint dogId) {
    require(dogs[dogId].owner == msg.sender, "Dog needs to belong to sender.");
    _;
  }

  /** @dev Modifier to check if dog exists. */
  modifier dogExists(uint dogId) {
    require(bytes(dogs[dogId].name).length != 0, "Dog does not exist");
    _;
  }

  event DogAdded(
    uint dogId
  );

  /** @dev Adds a dog for adoption.
    * @param name Name of the dog.
    * @param breed Breed of the dog.
    */
  function addDog(string name, string breed )
  public
  payable {
    dogs[idCounter] = Dog(msg.sender, name, breed, AdoptionStatus.Created);
    dogIds.push(idCounter);
    ownerDogs[msg.sender].push(idCounter);
    idCounter += 1;
    emit DogAdded(idCounter - 1);
  }


  /** @dev Updates status of the dog to "adopted".
    * @param dogId ID of dog.
    */
  function adoptDog(uint dogId )
  public
  payable {
    dogs[dogId].status = AdoptionStatus.Adopted;
  }


  /** @dev Get a specific dog by ID.
    * @param dogId ID of dog.
    * @return n Dog's name.
    * @return b Dog's breed.
    * @return s Adoption status of the dog.
    * @return o Dog owner's address.
    */
  function getDog(uint dogId)
  public
  view
  returns (string n, string b, string s, address o) {
    string memory status = "created";
    if (dogs[dogId].status == AdoptionStatus.Adopted) {
      status = "adopted";
    }
    return (dogs[dogId].name, dogs[dogId].breed, status, dogs[dogId].owner);
  }


  /** @dev Get all dog IDs who belongs to sender.
    * @return Dog IDs that belong to sender.
    */
  function getOwnerDogIds()
  public
  view
  returns (uint[]) {
    return ownerDogs[msg.sender];
  }


  /** @dev Gets all dog IDs in the Dapp.
    * @return All dog IDs.
    */
  function getAllDogIds()
  public
  view
  returns (uint[]) {
    return dogIds;
  }
  

  /** @dev Gets the total amount of dogs.
    * @return Number of dogs.
    */
  function getDogCount()
  public
  view
  returns (uint) {
    return dogIds.length;
  }

  event dogRemoved(
    uint dogId
  );

  /** @dev Removes dog with dog ID.
    * @param dogId ID of dog.
    */
  function removeDog(uint dogId)
  public
  dogExists(dogId)
  dogBelongsToSender(dogId)
  payable {
    for (uint i = 0; i < dogIds.length; ++i) {
      if (dogIds[i] == dogId) {
        remove(dogIds, i);    
      }
    }
    for (uint j = 0; j < ownerDogs[msg.sender].length; ++j) {
      if (ownerDogs[msg.sender][j] == dogId) {
        remove(ownerDogs[msg.sender], j);    
      }
    }
    delete dogs[dogId];
    emit dogRemoved(dogId);
  }

  function remove(uint[] storage array, uint index)  private returns(uint[]) {
    if (index >= array.length) return;

    for (uint i = index; i<array.length-1; i++){
      array[i] = array[i+1];
    }
    delete array[array.length-1];
    array.length--;
    return array;
  }
}

