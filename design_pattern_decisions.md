# Design Pattern Decisions
[https://courses.consensys.net/courses/course-v1:ConsenSysAcademy+2018DP+1/courseware/6ad0b7c56c8947a2b101cb916896fb9f/edd7e596883247cbb04e5145b3508467/?activate_block_id=block-v1%3AConsenSysAcademy%2B2018DP%2B1%2Btype%40sequential%2Bblock%40edd7e596883247cbb04e5145b3508467](Consensys Design Pattern Page)

## Fail early and fail loud
I have demonstrated use of require/modifiers to ensure that contracts don't get into a bad state.

## Restricting Access
I have also used Ownable with modifiers to restrict function to only those users who need to perform the function and would be less likely to be malicious with their intentions (as it is more likely they would be the victim). I have further been cautious with use of visibility modifiers, e.g. public/external/internal/private to restrict access as much as possible

## Inheritance/Library/EthPM
I have inherited from robust and code tested by experts (e.g. Zeppeling throught EthPM). This reduces duplication and means more of my code has been verified as safe by experts.

## Commenting
